var express = require('express');
var router = express.Router();
var contact = require('../bin/internal_contact.json').depart;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Home - Express MIS' });
});

router.get('/share', function (req, res) {
  res.render();
});

router.get('/contact', function (req, res) {
  res.render('internalContact', { title: 'Contact - Express MIS', contact: contact });
});
module.exports = router;
