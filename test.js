var config = require('./bin/configuration.json').sql.config.tempserver;
var Product = require('./model/product');

(async () => {
    let product = new Product(config);
    let str = product.select("Product", "ImportTime")
    .where("Product", "RMV000000030005")
    .orderby("ImportTime")
    .desc()
    .get();
    // .get();
    console.log(str.result);
})();