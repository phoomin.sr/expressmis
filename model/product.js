const SqlCommand = require('../bin/sql/SqlCommand');

class Product extends SqlCommand {
    constructor(config) {
        super(config);
        this.table = 'Product';
    }
}

module.exports = Product;