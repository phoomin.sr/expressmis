const sql  = require('mssql');

class SqlConnection {
    constructor() {
        this.conn;
        this.type;
    }
    async Connect (config) {
        await sql.connect(config)
        .then(connection => {
            this.conn = connection;
        })
        .catch(err => {
            console.log(err);
        });

        return this;
    }
    getSql() {
        return sql;
    }
}

module.exports = SqlConnection;