const SqlConnection = require('../sql/SqlConnection');

class SqlCommand extends SqlConnection {
    constructor(config) {
        super();
        this.config = config;

        this.table = "";
        this.method;
        this.query;
        this.stmt = "";
        this.Select = "SELECT { top }\r\n\t{ select }\r\nFROM\r\n\t{ table }"
        this.selColumn = "";
        this.condition = false;
        this.keys = [];
        this.Insert = "INSERT INTO { table } ({ column }) VALUES ({ values })";
        this.Update = "UPDATE { table } SET ({ values })";
        this.Delete = "DELETE FROM { table }";
        this.withTop = false;
        this.limit = 1000;
        this.result = {};
        this.Order = false;
        this.Orderby = "";
        this.Group = false;
        this.Groupby = "";
        this.Ascending = false;
        this.Descending = true;
        this.params = [];
    }
    select () {
        this.stmt = this.Select;
        if (arguments.length > 0 ) {
            for (let i = 0;i < arguments.length;i++) {
                this.selColumn += arguments[i];
                if ((i + 1) != arguments.length) {
                    this.selColumn += "\r\n\t,";
                }
            }
        } else {
            this.selColumn = "*";
        }

        return this;
    }
    top (limit) {
        this.withTop = true;
        this.limit = limit;
        return this;
    }
    setTable (table) {
        this.table = table;
        return this;
    }
    where() {
        this.condition = true;

        for (let i = 0;i < arguments.length;i++) {
            if ((i % 2) == 0) {
                this.keys.push(arguments[i]);
            } else {
                this.params.push(arguments[i]);
            }
        }

        return this;
    }
    groupby(column) {
        this.Group = true;

        if (arguments.length > 1) {
            for (let i = 0;i < arguments.length;i++) {
                this.Groupby += arguments[i];
                if ((i + 1) != arguments.length) {
                    this.Groupby += ", ";
                }
            }
        } else {
            this.Groupby = arguments[0];
        }

        return this;
    }
    orderby(column) {
        this.Order = true;
        if (arguments.length > 1) {
            for (let i = 0;i < arguments.length;i++) {
                this.Orderby += arguments[i];
                if ((i + 1) != arguments.length) {
                    this.Orderby += ", ";
                }
            }
        } else {
            this.Orderby = arguments[0];
        }
        return this;
    }
    asc() {
        this.Ascending = true;
        this.Descending = false;

        return this;
    }
    desc() {
        this.Ascending = false;
        this.Descending = true;

        return this;
    }
    find(column, value) {
        this.select().where();
        return this;
    }
    async get() {
        await this.Connect(this.config);
        this.createStatement();

        for (let i = 0;i < this.keys.length;i++) {
            this.result = await this.conn
            .request()
            .input(this.keys[i], this.getSql().Char, this.params[i])
        }
        
        // loop for add a parameter/parameters
        this.result = await this.result.query(`${this.stmt}`);
        await this.conn.close();

        return this;
    }
    first() {
        return this.result.recordset[0];
    }
    last() {
        return this.result.recordset[(this.result.recordset.length - 1)]
    }
    getCondition() {
        if (this.condition) {
            this.stmt += `\r\nWHERE `;
            
            for(let i = 0;i < this.keys.length;i++) {
                this.stmt += `\r\n\t${this.keys[i]} = @${this.keys[i]}`;
                if ((i + 1) != this.keys.length) {
                    this.stmt += ` AND `;
                }
            }
        }

        return this.stmt;
    }
    createStatement() {
        if (this.withTop) {
            this.stmt = this.stmt.replace('{ top }', `TOP(${this.limit})`);
        } else {
            this.stmt = this.stmt.replace('{ top }', '');
        }
        
        this.stmt = this.stmt.replace('{ select }', this.selColumn);
        this.stmt = this.stmt.replace('{ table }', this.table);
        this.stmt = this.getCondition();

        if (this.Group) {
            this.stmt += `\r\nGROUP BY ${this.Groupby}`
        }

        if (this.Order) {
            this.stmt += `\r\nORDER BY\r\n\t${this.Orderby}\r\n${this.Ascending ? 'ASC':'DESC'}`;
        }

        return this;
    }
    toString() {
        this.createStatement();
        return this.stmt;
    }
    destructor() {
        this.conn.close();
    }
}

module.exports = SqlCommand